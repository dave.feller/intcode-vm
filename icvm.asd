(defsystem "icvm"
  :description "Intcode VM"
  :author "A"
  :license "GPL V3"
  :version "0.0.1"
  :serial t
  :depends-on (:alexandria :str)
  :components ((:file "package")
               (:file "utils")
               (:file "icvm")))
