(defpackage icvm
  (:use :cl)
  (:import-from :alexandria
                :read-file-into-string
                :flatten
                :parse-body)
  (:import-from :str
                :split)
  (:export :mem
           :opc
           :eip
           :erb
           :exit-code
           :outfn
           :infn
           :cpu
           :halt
           :icm
           :parse-mem
           :*context*
           :run-program
           :make-icm
           :bind-icm))
